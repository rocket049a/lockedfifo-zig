# `zig`语言线程安全的`LockedFifo`
这些代码提供一个基于`zig`标准库`std.fifo.LinearFifo`创建的带锁的线程安全的`LockedFifo`类型，性质类似`go`语言中的`channel`，用于线程间的通讯。使用者通过调用函数`LockedFifo(comptime T: type, comptime cache_size: isize) type`获取该类型，具体用法可以参考`src/lockedfifo_test.zig`中的测试代码`lockedfifo_main()`和`receiver(...)`两个函数。

注意：`cache_size`参数应该大于`0`,如果小于（包含）`0`,将会自动设置成`10`。

运行`test`代码：`cd src && zig test lockedfifo_test.zig`

### `LockedFifo`提供的函数：

* 创建类型函数：`pub fn LockedFifo(comptime T: type, comptime cache_size: isize) type`
* 写入函数：`pub fn writeItem(self: *Self, item: T) !void`
* 读取函数：`pub fn readItem(self: *Self) !T`
* 关闭函数（关闭后不能写入，写入函数会返回错误，但可以继续读取到最后写入的值，接着读取会返回错误)：`pub fn close(self: *Self) void`
* 资源释放函数：`pub fn deinit(self: *Self) void`